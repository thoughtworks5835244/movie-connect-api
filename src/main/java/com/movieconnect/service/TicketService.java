package com.movieconnect.service;

import com.movieconnect.dto.MovieSessionResponse;
import com.movieconnect.dto.TicketResponse;
import com.movieconnect.entity.MovieSession;
import com.movieconnect.entity.Ticket;
import com.movieconnect.repository.TicketRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class TicketService {

    MovieSessionService sessionService;
    TicketRepository ticketRepository;

    public TicketService(
        MovieSessionService sessionService,
        TicketRepository ticketRepository
    ) {
        this.sessionService = sessionService;
        this.ticketRepository = ticketRepository;
    }

    public TicketResponse save(Ticket ticket) {
        return toResponse(ticketRepository.save(ticket));
    }

    public TicketResponse toResponse(Ticket ticket) {
        TicketResponse response = new TicketResponse();
        BeanUtils.copyProperties(ticket, response);

        MovieSession session = sessionService.findById(ticket.getSessionId());
        MovieSessionResponse sessionResponse = sessionService.toResponse(session);
        response.setSession(sessionResponse);
        return response;
    }

    public boolean hasSeatOccupied(Ticket ticket) {
        return ticketRepository
            .findBySessionIdAndSeat(ticket.getSessionId(), ticket.getSeat())
            .isPresent();
    }
}
