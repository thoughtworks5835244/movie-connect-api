package com.movieconnect;

import com.movieconnect.entity.*;
import com.movieconnect.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class MovieControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MovieSessionRepository movieSessionRepository;

    @BeforeEach
    void setup() {
        movieRepository.deleteAll();
        movieSessionRepository.deleteAll();
    }

    @Test
    void should_return_all_movies_when_get_all_movies() throws Exception {
        movieRepository.save(new Movie());
        movieRepository.save(new Movie());

        mockMvc
            .perform(get("/movies"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(2)))
        ;
    }

    @Test
    void should_return_movie_with_correct_id_when_get_movie_by_id() throws Exception {
        Movie movie = movieRepository.save(new Movie());

        mockMvc
            .perform(get("/movies/" + movie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(movie.getId().intValue())))
            .andExpect(jsonPath("$.title", is(movie.getTitle())))
        ;
    }

    @Test
    void should_return_all_movie_sessions_when_get_movie_sessions_by_movie_id() throws Exception {
        Movie movie = movieRepository.save(new Movie());

        MovieSession session1 = new MovieSession();
        session1.setMovieId(movie.getId());
        session1.setPrice(110.5F);
        session1.setCinema("Test Cinema1");
        session1.setTheater("Test Theater1");
        session1.setStartAt("2023-12-09T19:00:00+08:00");
        session1.setWatchMode("Companion");

        MovieSession session2 = new MovieSession();
        session2.setMovieId(movie.getId());
        session2.setPrice(125.5F);
        session2.setCinema("Test Cinema2");
        session2.setTheater("Test Theater2");
        session2.setStartAt("2023-12-09T19:00:00+08:00");
        session2.setWatchMode("Normal");

        MovieSession savedSession1 = movieSessionRepository.save(session1);
        MovieSession savedSession2 = movieSessionRepository.save(session2);

        mockMvc.
            perform(get("/movies/{id}/sessions", movie.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))

            .andExpect(jsonPath("$[0].id").value(savedSession1.getId()))
            .andExpect(jsonPath("$[0].theater").value(savedSession1.getTheater()))
            .andExpect(jsonPath("$[0].price").value(savedSession1.getPrice()))
            .andExpect(jsonPath("$[0].cinema").value(savedSession1.getCinema()))
            .andExpect(jsonPath("$[0].startAt").value(savedSession1.getStartAt()))
            .andExpect(jsonPath("$[0].watchMode").value(savedSession1.getWatchMode()))
            .andExpect(jsonPath("$[0].movieId").value(movie.getId()))

            .andExpect(jsonPath("$[1].id").value(savedSession2.getId()))
            .andExpect(jsonPath("$[1].theater").value(savedSession2.getTheater()))
            .andExpect(jsonPath("$[1].price").value(savedSession2.getPrice()))
            .andExpect(jsonPath("$[1].cinema").value(savedSession2.getCinema()))
            .andExpect(jsonPath("$[1].startAt").value(savedSession2.getStartAt()))
            .andExpect(jsonPath("$[1].watchMode").value(savedSession2.getWatchMode()))
            .andExpect(jsonPath("$[1].movieId").value(movie.getId()));
    }
}
