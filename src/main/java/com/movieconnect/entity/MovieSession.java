package com.movieconnect.entity;

import javax.persistence.*;

@Entity
@Table(name = "movie_session")
public class MovieSession {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long movieId;

    private Float price;
    private String cinema;
    private String theater;
    private String startAt;
    private String watchMode;

    public MovieSession() {
    }

    public MovieSession(
        Long movieId,
        Float price,
        String cinema,
        String theater,
        String startAt,
        String watchMode
    ) {
        this.movieId = movieId;
        this.price = price;
        this.cinema = cinema;
        this.theater = theater;
        this.startAt = startAt;
        this.watchMode = watchMode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCinema() {
        return cinema;
    }

    public void setCinema(String cinema) {
        this.cinema = cinema;
    }

    public String getTheater() {
        return theater;
    }

    public void setTheater(String theater) {
        this.theater = theater;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getWatchMode() {
        return watchMode;
    }

    public void setWatchMode(String watchMode) {
        this.watchMode = watchMode;
    }
}