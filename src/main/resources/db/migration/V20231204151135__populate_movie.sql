USE `movieConnect`;

DELETE FROM movie WHERE runtime is NULL;

ALTER TABLE movie AUTO_INCREMENT = 1;

INSERT INTO movie (title, poster_url, genres, runtime, plot) VALUES
(
    'Oppenheimer',
    'https://th.bing.com/th/id/OIP.r8xsiy88_gdZdve7JcGF8AHaLH?rs=1&pid=ImgDetMain',
    'history,biography',
    180,
    'A dramatization of the life story of J. Robert Oppenheimer, the physicist who had a large hand in the development of the atomic bomb.'
),
(
    'Spider-Man: Across the Spider-Verse',
    'https://image.tmdb.org/t/p/original/16HMAMQNceEKrY3odATphns0lm7.jpg',
    'action,adventure',
    140,
    'Miles Morales returns for epic adventure that will transform Brooklyn.'
),
(
    'Past Lives',
    'https://cdn.traileraddict.com/content/a24/past-lives-poster.jpg',
    'drama,romance',
    105,
    'Nora and Hae Sung, two deeply connected childhood friends, are wrested apart after Noras family emigrates from South Korea.'
),
(
    'John Wick: Chapter 4',
    'https://th.bing.com/th/id/R.754b88db780cbd0f059592c686e30b4e?rik=F1TPH8T%2f2dk5uw&pid=ImgRaw&r=0',
    'action,adventure',
    180,
    'John Wick uncovers a path to defeating The High Table.'
),
(
    'Wonka',
    'https://cdn.kinocheck.com/i/ba7lntlj81.jpg',
    'adventure,comedy',
    140,
    'With dreams of opening a shop in a city renowned for its chocolate.'
),
(
    'Barbie',
    'https://i.pinimg.com/originals/3c/d2/f0/3cd2f0cb0b5a783830278db005384a43.jpg',
    'adventure,comedy',
    180,
    'Barbie suffers a crisis that leads her to question her world and her existence.'
),
(
    'Napoleon',
    'https://th.bing.com/th/id/OIP.9oYhLepIJraZZjQdmfoq8wAAAA?rs=1&pid=ImgDetMain',
    'adventure,biography',
    200,
    'An epic that details the checkered rise and fall of French Emperor Napoleon.'
),
(
    'The Little Mermaid',
    'https://a.ltrbxd.com/resized/film-poster/3/7/9/5/9/1/379591-the-little-mermaid-0-460-0-690-crop.jpg?k=498b662b60',
    'adventure',
    180,
    'A young mermaid makes a deal with a sea witch to trade her beautiful voice for human legs.'
),
(
    'Guardians of the Galaxy Vol. 3',
    'https://th.bing.com/th/id/R.cb08cd7e13ced5eaaf3cbd975279072c?rik=q01g5MkRovbkZg&riu=http%3a%2f%2ftr.web.img2.acsta.net%2fpictures%2f19%2f08%2f24%2f20%2f41%2f5572826.jpg&ehk=E4mdAZ0KKI07IEMiyPGRiAciDcG5y5zp1eHWA3DFkLM%3d&risl=&pid=ImgRaw&r=0',
    'adventure,action',
    180,
    'Peter Quill rallies his team to defend the universe and one of their own.'
);