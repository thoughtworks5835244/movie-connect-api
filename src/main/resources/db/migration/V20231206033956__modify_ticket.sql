USE `movieConnect`;

ALTER TABLE         `ticket`
DROP FOREIGN KEY    `ticket_ibfk_1`,
DROP COLUMN         `movie_id`,
DROP COLUMN         `price`,
DROP COLUMN         `cinema`,
DROP COLUMN         `start_at`,
DROP COLUMN         `watch_mode`,
ADD COLUMN          `session_id`    bigint          NOT NULL,
ADD COLUMN          `seat`          varchar(255)    NOT NULL,
ADD CONSTRAINT      `fk_ticket_session`
FOREIGN KEY (`session_id`) REFERENCES `movie_session` (`id`);
