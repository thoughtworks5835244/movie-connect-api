package com.movieconnect.component;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    private static final String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

    public static String getCurrentDatetime() {
        var formatter = DateTimeFormatter.ofPattern(ISO_FORMAT);
        var now = OffsetDateTime.now(ZoneId.ofOffset("UTC", ZoneOffset.ofHours(8)));
        return now.format(formatter);
    }
}
