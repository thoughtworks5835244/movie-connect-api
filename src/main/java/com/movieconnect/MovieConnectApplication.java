package com.movieconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieConnectApplication.class, args);
	}
}
