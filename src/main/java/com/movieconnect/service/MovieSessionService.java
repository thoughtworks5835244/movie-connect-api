package com.movieconnect.service;

import com.movieconnect.dto.MovieSessionResponse;
import com.movieconnect.entity.Movie;
import com.movieconnect.entity.MovieSession;
import com.movieconnect.entity.Ticket;
import com.movieconnect.exception.NotFoundException;
import com.movieconnect.repository.MovieSessionRepository;
import com.movieconnect.repository.TicketRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieSessionService {

    MovieService movieService;
    MovieSessionRepository movieSessionRepository;
    TicketRepository ticketRepository;

    public MovieSessionService(
        MovieService movieService,
        MovieSessionRepository movieSessionRepository,
        TicketRepository ticketRepository
    ) {
        this.movieService = movieService;
        this.movieSessionRepository = movieSessionRepository;
        this.ticketRepository = ticketRepository;
    }

    public List<MovieSession> findAllByMovieId(Long movieId) {
        return movieSessionRepository.findAllByMovieId(movieId);
    }

    public MovieSession findById(Long id) {
        return movieSessionRepository
            .findById(id)
            .orElseThrow(() -> new NotFoundException("MovieSession not found"));
    }

    public MovieSessionResponse toResponse(MovieSession session) {
        MovieSessionResponse response = new MovieSessionResponse();
        BeanUtils.copyProperties(session, response);

        Movie movie = movieService.findById(session.getMovieId());
        response.setMovie(movie);
        return response;
    }

    public List<String> getSessionOccupiedSeats(Long id) {
        return ticketRepository
            .findAllBySessionId(id)
            .stream()
            .map(Ticket::getSeat)
            .sorted()
            .collect(Collectors.toList());
    }
}
