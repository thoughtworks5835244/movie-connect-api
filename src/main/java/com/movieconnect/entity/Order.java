package com.movieconnect.entity;

import com.movieconnect.component.DateUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "\"order\"")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(targetEntity = Ticket.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "orderId")
    private List<Ticket> tickets = new ArrayList<>();

    private String placedAt;

    public Order() {
        this.placedAt = DateUtils.getCurrentDatetime();
    }

    public Order(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Order(List<Ticket> tickets, String placedAt) {
        this.tickets = tickets;
        this.placedAt = placedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public String getPlacedAt() {
        return placedAt;
    }

    public void setPlacedAt(String placedAt) {
        this.placedAt = placedAt;
    }
}
