package com.movieconnect;

import com.movieconnect.entity.*;
import com.movieconnect.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class MovieSessionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MovieSessionRepository sessionRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    OrderRepository orderRepository;

    @BeforeEach
    void setup() {
        ticketRepository.deleteAll();
        orderRepository.deleteAll();
        sessionRepository.deleteAll();
        movieRepository.deleteAll();
    }

    @Test
    void should_return_all_occupied_seats_when_given_movie_session_id() throws Exception {
        var movie = movieRepository.save(new Movie("title1", "posterUrl1", "genres1", 1, "plot1"));
        var session = sessionRepository.save(new MovieSession(movie.getId(), 1.0f, "cinema1", "theater1", "2023-12-14T18:12:55+08:00", "Normal"));

        var order1 = orderRepository.save(new Order());
        ticketRepository.save(new Ticket(order1.getId(), session.getId(), "A1"));
        ticketRepository.save(new Ticket(order1.getId(), session.getId(), "B2"));

        var order2 = orderRepository.save(new Order());
        ticketRepository.save(new Ticket(order2.getId(), session.getId(), "C3"));
        ticketRepository.save(new Ticket(order2.getId(), session.getId(), "D4"));

        mockMvc
            .perform(get("/sessions/" + session.getId() + "/occupied-seats"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(4)))
            .andExpect(jsonPath("$[0]", is("A1")))
            .andExpect(jsonPath("$[1]", is("B2")))
            .andExpect(jsonPath("$[2]", is("C3")))
            .andExpect(jsonPath("$[3]", is("D4")))
        ;
    }
}
