package com.movieconnect;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movieconnect.entity.*;
import com.movieconnect.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class OrderControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MovieSessionRepository sessionRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    OrderRepository orderRepository;

    Gson gson = new GsonBuilder().create();

    @BeforeEach
    void setup() {
        ticketRepository.deleteAll();
        orderRepository.deleteAll();
        sessionRepository.deleteAll();
        movieRepository.deleteAll();
    }

    @Test
    void should_return_all_orders_contains_all_tickets_and_all_movies_when_get_all_orders() throws Exception {
        var movie = movieRepository.save(new Movie("title1", "posterUrl1", "genres1", 1, "plot1"));
        var session = sessionRepository.save(new MovieSession(movie.getId(), 1.0f, "cinema1", "theater1", "2023-12-14T18:12:55+08:00", "Normal"));
        var order = orderRepository.save(new Order());
        var ticket = ticketRepository.save(new Ticket(order.getId(), session.getId(), "A1"));

        mockMvc
            .perform(get("/orders"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(1)))
            .andExpect(jsonPath("$[0].id", is(order.getId().intValue())))
            .andExpect(jsonPath("$[0].placedAt", is(order.getPlacedAt())))
            .andExpect(jsonPath("$[0].tickets", hasSize(1)))
            .andExpect(jsonPath("$[0].tickets[0].id", is(ticket.getId().intValue())))
            .andExpect(jsonPath("$[0].tickets[0].seat", is(ticket.getSeat())))

            .andExpect(jsonPath("$[0].tickets[0].session", is(notNullValue())))
            .andExpect(jsonPath("$[0].tickets[0].session.id", is(session.getId().intValue())))
            .andExpect(jsonPath("$[0].tickets[0].session.price", is(session.getPrice().doubleValue())))
            .andExpect(jsonPath("$[0].tickets[0].session.cinema", is(session.getCinema())))
            .andExpect(jsonPath("$[0].tickets[0].session.theater", is(session.getTheater())))
            .andExpect(jsonPath("$[0].tickets[0].session.startAt", is(session.getStartAt())))
            .andExpect(jsonPath("$[0].tickets[0].session.watchMode", is(session.getWatchMode())))

            .andExpect(jsonPath("$[0].tickets[0].session.movie", is(notNullValue())))
            .andExpect(jsonPath("$[0].tickets[0].session.movie.id", is(movie.getId().intValue())))
            .andExpect(jsonPath("$[0].tickets[0].session.movie.title", is(movie.getTitle())))
            .andExpect(jsonPath("$[0].tickets[0].session.movie.posterUrl", is(movie.getPosterUrl())))
            .andExpect(jsonPath("$[0].tickets[0].session.movie.genres", is(movie.getGenres())))
            .andExpect(jsonPath("$[0].tickets[0].session.movie.runtime", is(movie.getRuntime())))
            .andExpect(jsonPath("$[0].tickets[0].session.movie.plot", is(movie.getPlot())))
        ;
    }

    @Test
    void should_save_order_and_ticket_when_given_a_empty_order_with_a_ticket() throws Exception {
        var movie = movieRepository.save(new Movie("title1", "posterUrl1", "genres1", 1, "plot1"));
        var session = sessionRepository.save(new MovieSession(movie.getId(), 1.0f, "cinema1", "theater1", "2023-12-14T18:12:55+08:00", "Normal"));

        Ticket ticket = new Ticket(session.getId(), "B6");
        Order order = new Order(List.of(ticket));

        mockMvc
            .perform(
                post("/orders")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(gson.toJson(order))
            )
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.placedAt", is(notNullValue())))
            .andExpect(jsonPath("$.tickets", hasSize(1)))
            .andExpect(jsonPath("$.tickets[0].seat", is(ticket.getSeat())))

            .andExpect(jsonPath("$.tickets[0].session", is(notNullValue())))
            .andExpect(jsonPath("$.tickets[0].session.price", is(session.getPrice().doubleValue())))
            .andExpect(jsonPath("$.tickets[0].session.cinema", is(session.getCinema())))
            .andExpect(jsonPath("$.tickets[0].session.theater", is(session.getTheater())))
            .andExpect(jsonPath("$.tickets[0].session.startAt", is(session.getStartAt())))
            .andExpect(jsonPath("$.tickets[0].session.watchMode", is(session.getWatchMode())))

            .andExpect(jsonPath("$.tickets[0].session.movie", is(notNullValue())))
            .andExpect(jsonPath("$.tickets[0].session.movie.title", is(movie.getTitle())))
            .andExpect(jsonPath("$.tickets[0].session.movie.posterUrl", is(movie.getPosterUrl())))
            .andExpect(jsonPath("$.tickets[0].session.movie.genres", is(movie.getGenres())))
            .andExpect(jsonPath("$.tickets[0].session.movie.runtime", is(movie.getRuntime())))
            .andExpect(jsonPath("$.tickets[0].session.movie.plot", is(movie.getPlot())))
        ;

        assertEquals(1, ticketRepository.count());
        assertEquals(1, orderRepository.count());
    }
}
