USE `movieConnect`;

CREATE TABLE IF NOT EXISTS `movie_session`
(
    `id`            bigint       NOT NULL AUTO_INCREMENT,
    `price`         float        NOT NULL,
    `cinema`        varchar(255) NOT NULL,
    `theater`       varchar(255) NOT NULL,
    `start_at`      varchar(255) NOT NULL,
    `watch_mode`    varchar(255) NOT NULL,
    `movie_id`      bigint       NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`)
)
;

INSERT INTO `movie_session` (price, cinema, theater, start_at, watch_mode, movie_id) VALUES
(
    90,
    'MCL Cinema k11',
    'H5',
    '2023-12-09T19:00:00+08:00',
    'normal',
    1
),
(
    120,
    'MCL Cinema k11',
    'D4',
    '2023-12-09T15:00:00+08:00',
    'companion',
    1
),
(
    120,
    'MCL Cinema k11',
    'T6',
    '2023-12-09T15:00:00+08:00',
    'companion',
    1
),
(
    100,
    'MCL Cinema k11',
    'D4',
    '2023-12-14T13:00:00+08:00',
    'normal',
    2
),
(
    90,
    'MCL Cinema k11',
    'D4',
    '2023-12-16T09:00:00+08:00',
    'normal',
    3
),
(
    130,
    'MCL Cinema k11',
    'D4',
    '2023-12-15T11:00:00+08:00',
    'normal',
    4
),
(
    100,
    'Broadway MOKO',
    'B',
    '2023-12-10T14:00:00+08:00',
    'normal',
    1
),
(
    100,
    'Broadway MOKO',
    'A',
    '2023-12-13T12:00:00+08:00',
    'normal',
    3
),
(
    130,
    'Broadway MOKO',
    'C',
    '2023-12-13T14:00:00+08:00',
    'companion',
    5
),
(
    100,
    'Broadway MOKO',
    'A',
    '2023-12-13T11:00:00+08:00',
    'normal',
    7
),
(
    80,
    'Cinema City',
    '3',
    '2023-12-12T10:00:00+08:00',
    'companion',
    1
),
(
    100,
    'Cinema City',
    '4',
    '2023-12-13T12:00:00+08:00',
    'normal',
    2
),
(
    110,
    'Cinema City',
    '2',
    '2023-12-13T16:00:00+08:00',
    'companion',
    6
),
(
    100,
    'Cinema City',
    '1',
    '2023-12-13T10:00:00+08:00',
    'companion',
    8
),
(
    120,
    'Cinema City',
    '5',
    '2023-12-13T20:00:00+08:00',
    'normal',
    9
);
