package com.movieconnect.controller;

import com.movieconnect.entity.Movie;
import com.movieconnect.entity.MovieSession;
import com.movieconnect.service.MovieService;
import com.movieconnect.service.MovieSessionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {

    MovieService movieService;
    MovieSessionService movieSessionService;

    public MovieController(
        MovieService movieService,
        MovieSessionService movieSessionService
    ) {
        this.movieService = movieService;
        this.movieSessionService = movieSessionService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Movie> getMovies() {
        return movieService.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Movie getMovieById(@PathVariable Long id) {
        return movieService.findById(id);
    }

    @GetMapping("/{id}/sessions")
    @ResponseStatus(HttpStatus.OK)
    public List<MovieSession> getMovieSessionsByMovieId(@PathVariable Long id) {
        return movieSessionService.findAllByMovieId(id);
    }
}
