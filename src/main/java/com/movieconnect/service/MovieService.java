package com.movieconnect.service;

import com.movieconnect.entity.Movie;
import com.movieconnect.exception.NotFoundException;
import com.movieconnect.repository.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {

    MovieRepository repo;

    public MovieService(MovieRepository repo) {
        this.repo = repo;
    }

    public List<Movie> findAll() {
        return repo.findAll();
    }

    public Movie findById(Long id) {
        return repo
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Movie not found"));
    }
}
