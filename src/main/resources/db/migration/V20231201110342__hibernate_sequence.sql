USE `movieConnect`;

CREATE TABLE IF NOT EXISTS `hibernate_sequence`
(
    `id`       bigint NOT NULL AUTO_INCREMENT,
    `next_val` bigint NOT NULL,
    PRIMARY KEY (`id`)
)
;

INSERT INTO hibernate_sequence (next_val)
VALUES (1);
