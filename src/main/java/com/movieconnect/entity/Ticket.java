package com.movieconnect.entity;

import javax.persistence.*;

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long orderId;

    private Long sessionId;

    private String seat;

    public Ticket() {
    }

    public Ticket(
        Long sessionId,
        String seat
    ) {
        this.sessionId = sessionId;
        this.seat = seat;
    }

    public Ticket(
        Long orderId,
        Long sessionId,
        String seat
    ) {
        this.orderId = orderId;
        this.sessionId = sessionId;
        this.seat = seat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }
}
