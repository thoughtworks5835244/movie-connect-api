USE `movieConnect`;

ALTER TABLE `movie`
    ADD `poster_url` varchar(255);

ALTER TABLE `movie`
    ADD `genres` varchar(255);

ALTER TABLE `movie`
    ADD `runtime` integer;

ALTER TABLE `movie`
    ADD `plot` varchar(255);

CREATE TABLE IF NOT EXISTS `order`
(
    `id`        bigint       NOT NULL AUTO_INCREMENT,
    `placed_at` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
)
;

CREATE TABLE IF NOT EXISTS `ticket`
(
    `id`       bigint       NOT NULL AUTO_INCREMENT,
    `movie_id` bigint       NOT NULL,
    `order_id` bigint       NOT NULL,
    `price`    float        NOT NULL,
    `cinema`   varchar(255) NOT NULL,
    `start_at` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`),
    FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
)
;
