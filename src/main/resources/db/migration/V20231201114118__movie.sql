USE `movieConnect`;

CREATE TABLE IF NOT EXISTS `movie`
(
    `id`    bigint       NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
)
;

INSERT INTO `movie` (`title`) VALUES ('Napoleon');
INSERT INTO `movie` (`title`) VALUES ('Barbie');
INSERT INTO `movie` (`title`) VALUES ('Spider-Man: Across the Spider-verse');
INSERT INTO `movie` (`title`) VALUES ('Guardians of the Galaxy Vol. 3');
INSERT INTO `movie` (`title`) VALUES ('The Little Mermaid');
INSERT INTO `movie` (`title`) VALUES ('Oppenheimer');
INSERT INTO `movie` (`title`) VALUES ('Ant-Man and the Wasp: Quantumania');
INSERT INTO `movie` (`title`) VALUES ('John Wick: Chapter 4');
INSERT INTO `movie` (`title`) VALUES ('Wonka');
INSERT INTO `movie` (`title`) VALUES ('The Hunger Games: The Ballad of Songbirds and Snakes');
INSERT INTO `movie` (`title`) VALUES ('Monster');
INSERT INTO `movie` (`title`) VALUES ('Fallen Leaves');
