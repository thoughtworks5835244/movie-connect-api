package com.movieconnect.dto;

public class TicketResponse {

    private Long id;
    private String seat;
    private MovieSessionResponse session;

    public TicketResponse() {
    }

    public TicketResponse(
        Long id,
        String seat,
        MovieSessionResponse session
    ) {
        this.id = id;
        this.seat = seat;
        this.session = session;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public MovieSessionResponse getSession() {
        return session;
    }

    public void setSession(MovieSessionResponse session) {
        this.session = session;
    }
}
