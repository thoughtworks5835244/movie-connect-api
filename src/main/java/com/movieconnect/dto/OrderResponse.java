package com.movieconnect.dto;

import java.util.List;

public class OrderResponse {

    private Long id;
    private String placedAt;
    private List<TicketResponse> tickets;

    public OrderResponse() {
    }

    public OrderResponse(
        Long id,
        String placedAt,
        List<TicketResponse> tickets
    ) {
        this.id = id;
        this.placedAt = placedAt;
        this.tickets = tickets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlacedAt() {
        return placedAt;
    }

    public void setPlacedAt(String placedAt) {
        this.placedAt = placedAt;
    }

    public List<TicketResponse> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketResponse> tickets) {
        this.tickets = tickets;
    }
}
