package com.movieconnect.dto;

import com.movieconnect.entity.Movie;

public class MovieSessionResponse {

    private Long id;
    private Movie movie;
    private Float price;
    private String cinema;
    private String theater;
    private String startAt;
    private String watchMode;

    public MovieSessionResponse() {
    }

    public MovieSessionResponse(
        Long id,
        Movie movie,
        Float price,
        String cinema,
        String theater,
        String startAt,
        String watchMode
    ) {
        this.id = id;
        this.movie = movie;
        this.price = price;
        this.cinema = cinema;
        this.theater = theater;
        this.startAt = startAt;
        this.watchMode = watchMode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCinema() {
        return cinema;
    }

    public void setCinema(String cinema) {
        this.cinema = cinema;
    }

    public String getTheater() {
        return theater;
    }

    public void setTheater(String theater) {
        this.theater = theater;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getWatchMode() {
        return watchMode;
    }

    public void setWatchMode(String watchMode) {
        this.watchMode = watchMode;
    }
}
