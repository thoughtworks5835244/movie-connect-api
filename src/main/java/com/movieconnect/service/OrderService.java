package com.movieconnect.service;

import com.movieconnect.dto.OrderResponse;
import com.movieconnect.dto.TicketResponse;
import com.movieconnect.entity.Order;
import com.movieconnect.entity.Ticket;
import com.movieconnect.exception.InvalidOrderException;
import com.movieconnect.exception.NotFoundException;
import com.movieconnect.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    TicketService ticketService;
    OrderRepository orderRepository;

    public OrderService(
        TicketService ticketService,
        OrderRepository orderRepository
    ) {
        this.ticketService = ticketService;
        this.orderRepository = orderRepository;
    }

    public List<OrderResponse> findAll() {
        return orderRepository
            .findAll()
            .stream()
            .map(order ->
                new OrderResponse(
                    order.getId(),
                    order.getPlacedAt(),
                    order.getTickets()
                        .stream()
                        .map(ticketService::toResponse)
                        .collect(Collectors.toList())
                )
            )
            .sorted(Comparator.comparing(OrderResponse::getPlacedAt).reversed())
            .collect(Collectors.toList());
    }

    public Order findById(Long id) {
        return orderRepository
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Order not found"));
    }

    public OrderResponse save(Order order) {
        if (!isValidOrder(order)) return null;

        Order savedOrder = orderRepository.save(new Order());
        List<TicketResponse> tickets = order.getTickets()
            .stream()
            .map(ticket -> {
                ticket.setOrderId(savedOrder.getId());
                return ticketService.save(ticket);
            })
            .collect(Collectors.toList());

        return new OrderResponse(
            savedOrder.getId(),
            savedOrder.getPlacedAt(),
            tickets
        );
    }

    private boolean isValidOrder(Order order) {
        if (order == null) {
            throw new InvalidOrderException("Order must not be null");
        }

        if (order.getTickets() == null || order.getTickets().isEmpty()) {
            throw new InvalidOrderException("Order must have at least 1 ticket");
        }

        boolean hasNullSessionId = order.getTickets()
            .stream()
            .anyMatch(ticket -> ticket.getSessionId() == null);

        if (hasNullSessionId) {
            throw new InvalidOrderException("Ticket must have sessionId");
        }

        boolean hasNullSeat = order.getTickets()
            .stream()
            .anyMatch(ticket -> ticket.getSeat() == null || ticket.getSeat().isEmpty());

        if (hasNullSeat) {
            throw new InvalidOrderException("Ticket must have seat");
        }

        List<String> occupiedSeats = order.getTickets()
            .stream()
            .filter(ticketService::hasSeatOccupied)
            .map(Ticket::getSeat)
            .sorted()
            .collect(Collectors.toList());

        if (!occupiedSeats.isEmpty()) {
            throw new InvalidOrderException(
                "Following ticket seat is occupied: " + occupiedSeats
            );
        }

        return true;
    }
}
