package com.movieconnect.controller;

import com.movieconnect.service.MovieSessionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sessions")
public class MovieSessionController {

    MovieSessionService movieSessionService;

    public MovieSessionController(MovieSessionService movieSessionService) {
        this.movieSessionService = movieSessionService;
    }

    @GetMapping("/{id}/occupied-seats")
    @ResponseStatus(HttpStatus.OK)
    public List<String> getSessionOccupiedSeats(@PathVariable Long id) {
        return movieSessionService.getSessionOccupiedSeats(id);
    }
}
